# Copyright (c) 2018-2020 Viktor Kireev
# Distributed under the MIT License

include_guard(DIRECTORY)

include(Molurus.Structure.Root)

set(Molurus.Structure.Include.dir ${Molurus.Structure.Root.dir}/include)
set(Molurus.Structure.Include.fileMaskList *.h *.hpp)
