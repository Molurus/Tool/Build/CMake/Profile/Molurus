# Copyright (c) 2018-2020 Viktor Kireev
# Distributed under the MIT License

include_guard(GLOBAL)

macro(molurus_module_declare moduleName)
  set(MODULE_TARGET ${moduleName})

  string(REPLACE . / MODULE_TARGET_PATH_PREFIX ${MODULE_TARGET})
  string(REPLACE . _ MODULE_TARGET_SNAKE ${MODULE_TARGET})
  string(TOUPPER ${MODULE_TARGET_SNAKE} MODULE_TARGET_CAPS_SNAKE)
endmacro()

macro(molurus_module_add_shared_option)
  option(${MODULE_TARGET_CAPS_SNAKE}_BUILD_SHARED
         "Build the ${MODULE_TARGET} as shared library")
endmacro()

macro(molurus_module_define_build_type)
  if (${MODULE_TARGET_CAPS_SNAKE}_BUILD_SHARED)
    set(MODULE_TARGET_BUILD_TYPE SHARED)
  else()
    set(MODULE_TARGET_BUILD_TYPE STATIC)
  endif()
endmacro()

macro(molurus_module_add_interface_library)
  add_library(${MODULE_TARGET} INTERFACE)
endmacro()

macro(molurus_module_add_library)
  add_library(${MODULE_TARGET} ${MODULE_TARGET_BUILD_TYPE})
endmacro()

macro(molurus_module_add_executable)
  if (WIN32)
    add_executable(${MODULE_TARGET} WIN32)
  else()
    add_executable(${MODULE_TARGET})
  endif()
endmacro()

macro(molurus_module_setup_language stdVersion)
  set_target_properties(${MODULE_TARGET} PROPERTIES LINKER_LANGUAGE CXX)
  target_compile_features(${MODULE_TARGET} PUBLIC cxx_std_${stdVersion})
endmacro()

macro(molurus_module_setup_export)
  set_target_properties(${MODULE_TARGET} PROPERTIES
    CXX_VISIBILITY_PRESET     hidden
    VISIBILITY_INLINES_HIDDEN true)

  include(GenerateExportHeader)

  generate_export_header(${MODULE_TARGET}
    BASE_NAME        ${MODULE_TARGET_CAPS_SNAKE}
    EXPORT_FILE_NAME ${CMAKE_CURRENT_BINARY_DIR}/include/${MODULE_TARGET_PATH_PREFIX}/Export.h)
endmacro()

macro(molurus_module_setup_install)
  install(TARGETS ${MODULE_TARGET}
          EXPORT  ${MODULE_TARGET}-targets)
endmacro()

macro(add_molurus_interface_library moduleName)
  molurus_module_declare(${moduleName})
  molurus_module_add_interface_library()
  molurus_module_include_directories(INTERFACE)
  molurus_module_source_group(Include)
  molurus_module_setup_install()
endmacro()

macro(add_molurus_library moduleName stdVersion)
  molurus_module_declare(${moduleName})
  molurus_module_add_shared_option()
  molurus_module_define_build_type()
  molurus_module_add_library()
  molurus_module_setup_language(${stdVersion})
  molurus_module_include_directories(PUBLIC)
  molurus_module_setup_export()
  molurus_module_setup_install()
endmacro()

macro(add_molurus_executable moduleName stdVersion)
  molurus_module_declare(${moduleName})
  molurus_module_add_executable()
  molurus_module_setup_language(${stdVersion})
  molurus_module_include_directories(PUBLIC)
  molurus_module_setup_install()
endmacro()

macro(molurus_module_include_directories scope)
  include(Molurus.Structure.Include)
  target_include_directories(${MODULE_TARGET} ${scope}
    $<BUILD_INTERFACE:${Molurus.Structure.Include.dir}>
    $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}/include>)
endmacro()

macro(molurus_module_sources scope)
  foreach(source IN ITEMS ${ARGN})
    if(NOT DEFINED Molurus.Structure.${source}.files)
      include(Molurus.Structure.AutoScan.${source})
    endif()
    target_sources(${MODULE_TARGET} ${scope}
      ${Molurus.Structure.${source}.files})
  endforeach()
endmacro()

macro(molurus_module_source_group)
  foreach(source IN ITEMS ${ARGN})
    if(NOT DEFINED Molurus.Structure.${source}.files)
      include(Molurus.Structure.AutoScan.${source})
    endif()
    source_group(TREE  ${Molurus.Structure.${source}.dir}
                 FILES ${Molurus.Structure.${source}.files})
  endforeach()
endmacro()

macro(molurus_module_autouic_search_paths)
  set_target_properties(${MODULE_TARGET} PROPERTIES
    AUTOUIC_SEARCH_PATHS "${Molurus.Structure.Qt.Ui.searchPaths}")
endmacro()

macro(molurus_module_use_automoc)
  set_target_properties(${MODULE_TARGET} PROPERTIES AUTOMOC TRUE)
endmacro()

macro(molurus_module_use_autouic)
  set_target_properties(${MODULE_TARGET} PROPERTIES AUTOUIC TRUE)
  molurus_module_autouic_search_paths()
endmacro()

function(molurus_module_propagate_pic_from_target _target)
  get_target_property(has_pic ${_target} POSITION_INDEPENDENT_CODE)
  if (has_pic)
    get_target_property(libList ${_target} LINK_LIBRARIES)
    foreach (_lib IN ITEMS ${libList})
      if (TARGET ${_lib})
        get_target_property(_lib_type ${_lib} TYPE)
        if (${_lib_type} STREQUAL "STATIC_LIBRARY")
          set_target_properties(${_lib} PROPERTIES
                                POSITION_INDEPENDENT_CODE True)
          molurus_module_propagate_pic_from_target(${_lib})
        endif()
      endif()
    endforeach()
  endif()
endfunction()

macro(molurus_module_propagate_pic)
  molurus_module_propagate_pic_from_target(${MODULE_TARGET})
endmacro()
