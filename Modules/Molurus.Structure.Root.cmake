# Copyright (c) 2018-2020 Viktor Kireev
# Distributed under the MIT License

include_guard(DIRECTORY)

set(Molurus.Structure.Root.dir ${CMAKE_CURRENT_SOURCE_DIR})
