# Copyright (c) 2018-2020 Viktor Kireev
# Distributed under the MIT License

include_guard(DIRECTORY)

include(Molurus.Structure.Root)

set(Molurus.Structure.Source.dir ${Molurus.Structure.Root.dir}/src)
set(Molurus.Structure.Source.fileMaskList *.h *.hpp *.c *.cpp)
