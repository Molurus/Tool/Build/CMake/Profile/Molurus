# Copyright (c) 2018-2020 Viktor Kireev
# Distributed under the MIT License

include_guard(DIRECTORY)

include(Molurus.Structure.Root)

set(Molurus.Structure.Qt.Ui.dir ${Molurus.Structure.Root.dir}/ui)
set(Molurus.Structure.Qt.Ui.fileMaskList *.ui)
