# Copyright (c) 2018-2020 Viktor Kireev
# Distributed under the MIT License

include_guard(DIRECTORY)

include(Molurus.Structure.Source)
include(Molurus.Tool)

molurus_tool_scan_files(${Molurus.Structure.Source.dir}
                       "${Molurus.Structure.Source.fileMaskList}"
                       Molurus.Structure.Source.files)
