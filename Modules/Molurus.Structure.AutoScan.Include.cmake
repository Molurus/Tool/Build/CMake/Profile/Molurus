# Copyright (c) 2018-2020 Viktor Kireev
# Distributed under the MIT License

include_guard(DIRECTORY)

include(Molurus.Structure.Include)
include(Molurus.Tool)

molurus_tool_scan_files(${Molurus.Structure.Include.dir}
                       "${Molurus.Structure.Include.fileMaskList}"
                       Molurus.Structure.Include.files)
