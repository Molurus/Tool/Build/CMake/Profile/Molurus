# Copyright (c) 2018-2020 Viktor Kireev
# Distributed under the MIT License

include_guard(GLOBAL)

function(molurus_tool_make_globbing
         prefix fileMaskList result)
  foreach(fileMask IN ITEMS ${fileMaskList})
    list(APPEND res ${prefix}/${fileMask})
  endforeach()
  set(${result} ${res} PARENT_SCOPE)
endfunction()

function(molurus_tool_normalize_paths fileList result)
  foreach(f IN ITEMS ${fileList})
    get_filename_component(absPath ${f} ABSOLUTE)
    list(APPEND res ${absPath})
  endforeach()
  set(${result} ${res} PARENT_SCOPE)
endfunction()

function(molurus_tool_scan_files prefix fileMaskList result)
  molurus_tool_make_globbing(${prefix} "${fileMaskList}" fileGlobbing)
  file(GLOB_RECURSE files ${fileGlobbing})
  molurus_tool_normalize_paths("${files}" normalFiles)
  set(${result} ${normalFiles} PARENT_SCOPE)
endfunction()

function(molurus_tool_make_search_paths fileList result)
  foreach(f IN ITEMS ${fileList})
    get_filename_component(dir ${f} DIRECTORY)
    get_filename_component(absDir ${dir} ABSOLUTE)
    list(APPEND res ${absDir})
  endforeach()
  list(REMOVE_DUPLICATES res)
  set(${result} ${res} PARENT_SCOPE)
endfunction()
