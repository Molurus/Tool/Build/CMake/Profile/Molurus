# Copyright (c) 2018-2020 Viktor Kireev
# Distributed under the MIT License

include_guard(DIRECTORY)

include(Molurus.Structure.Qt.Ui)
include(Molurus.Tool)

molurus_tool_scan_files(${Molurus.Structure.Qt.Ui.dir}
                       "${Molurus.Structure.Qt.Ui.fileMaskList}"
                       Molurus.Structure.Qt.Ui.files)

molurus_tool_make_search_paths("${Molurus.Structure.Qt.Ui.files}"
                               Molurus.Structure.Qt.Ui.searchPaths)
